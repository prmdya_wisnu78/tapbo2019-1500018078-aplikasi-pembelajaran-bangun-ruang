package com.company;

public class Kubus {
    double sisi;
    double lpkubus;
    double volkubus;

    public double getSisi() {
        return sisi;
    }

    public void setSisi(double sisi) {
        this.sisi = sisi;
    }

    public double getLpkubus() {
        return 6 * getSisi();
    }

    public double getVolkubus() {
        return getSisi() * getSisi() * getSisi();
    }
}
