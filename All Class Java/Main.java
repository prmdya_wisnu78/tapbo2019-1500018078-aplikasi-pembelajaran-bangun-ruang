package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here


        Scanner inputpilih = new Scanner(System.in);
        System.out.println("    ==Menu Perhitungan Dalam Aplikasi== ");
        System.out.println("1. Perhitungan Bangun Kubus                 2. Perhitungan Bangun Balok");
        System.out.println("3. Perhitungan Bangun Prisma(Segitiga)      4. Perhitungan Bangun Bola");
        System.out.println("5. Perhitungan Bangun Limas(Alas Persegi)   6. Perhitungan Bangun Tabung");
        System.out.println("7. Perhitungan Bangun Kerucut               8. Keluar");
        System.out.println("Masukkan Pilihan Anda : ");
        int pilih = inputpilih.nextInt();
        switch (pilih) {
            case 1:
                System.out.println("==Ini merupakan Bagian Perhitungan Kubus==");
                Kubus kub = new Kubus();
                Scanner inputkub = new Scanner(System.in);
                System.out.println("Masukkan Bilangan Sisi Kubus : ");
                kub.setSisi(inputkub.nextDouble());
                System.out.println("Luas Permukaan Kubus = " + kub.getLpkubus());
                System.out.println("Volume Kubus = " + kub.getVolkubus());
                System.out.println("=================================================");
                System.out.println("");
                break;
            case 2:
                System.out.println("==Ini merupakan Bagian Perhitungan Balok==");
                Balok bal = new Balok();
                Scanner inputbal = new Scanner(System.in);
                System.out.println("Masukkan Bilangan Panjang Balok : ");
                bal.setPbalok(inputbal.nextDouble());
                System.out.println("Masukkan Bilangan Lebar Balok : ");
                bal.setLbalok(inputbal.nextDouble());
                System.out.println("Masukkan Bilangan Tinggi Balok : ");
                bal.setTbalok(inputbal.nextDouble());
                System.out.println("Luas Permukaan Balok = " + bal.getLpbalok());
                System.out.println("Volume Balok = " + bal.getVolbalok());
                System.out.println("=================================================");
                System.out.println("");
                break;
            case 3:
                System.out.println("==Ini Merupakan Bagian Perhitungan Prisma (Segitiga)==");
                Prisma prism = new Prisma();
                Scanner inputprism = new Scanner(System.in);
                System.out.println("Masukkan Sisi A Segitiga : ");
                prism.setSalasa(inputprism.nextDouble());
                System.out.println("Masukkan Sisi B Segitiga : ");
                prism.setSalasb(inputprism.nextDouble());
                System.out.println("Masukkan Tinggi Prisma : ");
                prism.setTprisma(inputprism.nextDouble());
                System.out.println("Luas Alas Prisma = " + prism.getLalasprisma());
                System.out.println("Luas Permukaan Prisma = " + prism.getLpprisma());
                System.out.println("Volume Prisma = " + prism.getVolprisma());
                System.out.println("======================================================");
                System.out.println("");
                break;
            case 4:
                System.out.println("==Ini merupakan Bagian Perhitungan Bola==");
                Bola bola = new Bola();
                Scanner inputbola = new Scanner(System.in);
                System.out.println("Masukkan Jari - Jari Bola : ");
                bola.setJaribola(inputbola.nextDouble());
                System.out.println("Luas Permukaan Bola = " + bola.getLpbola());
                System.out.println("Volume Bola = " + bola.getVolbola());
                System.out.println("====================================================");
                System.out.println("");
                break;
            case 5:
                System.out.println("==Ini Merupakan Bagian Perhitungan Limas(Alas Persegi)==");
                Limas lims = new Limas();
                Scanner inputlims = new Scanner(System.in);
                System.out.println("Masukkan Sisi Alas Limas : ");
                lims.setSalaslimas(inputlims.nextDouble());
                System.out.println("Masukkan Sisi Segitiga Limas : ");
                lims.setSsegitigab(inputlims.nextDouble());
                System.out.println("Masukkan Tinggi Limas : ");
                lims.setTlimas(inputlims.nextDouble());
                System.out.println("Luas Atap Limas = " + lims.getLsegitiga());
                System.out.println("Luas Permukaan Limas = " + lims.getLplimas());
                System.out.println("Volume Limas = " + lims.getVollimas());
                System.out.println("==========================================================");
                System.out.println("");
                break;
            case 6:
                System.out.println("==Ini Merupakan Bagian Perhitungan Tabung==");
                Tabung tabung = new Tabung();
                Scanner inputtabung = new Scanner(System.in);
                System.out.println("Masukkan Jari - Jari Tabung : ");
                tabung.setJaritabung(inputtabung.nextDouble());
                System.out.println("Masukkan Tinggi Tabung : ");
                tabung.setTinggi(inputtabung.nextDouble());
                System.out.println("Luas Permukaan Tabung = " + tabung.getLptabung());
                System.out.println("Volume Tabung = " + tabung.getVoltabung());
                System.out.println("=============================================================");
                System.out.println("");
                break;
            case 7:
                System.out.println("==Ini Merupakan Bagian Perhitungan Kerucut==");
                Kerucut kerucut = new Kerucut();
                Scanner inputkerucut = new Scanner(System.in);
                System.out.println("Masukkan Jari - Jari Kerucut : ");
                kerucut.setJarikerucut(inputkerucut.nextDouble());
                System.out.println("Masukkan Tinggi Kerucut : ");
                kerucut.setTinggikerucut(inputkerucut.nextDouble());
                System.out.println("Luas Selimut Kerucut = " + kerucut.getSelimutkerucut());
                System.out.println("Luas Permukaan Kerucut = " + kerucut.getLpkerucut());
                System.out.println("Volume Kerucut = " + kerucut.getVolkerucut());
                System.out.println("====================================================");
                System.out.println("");
                break;
            case 8:
            default:
        }
    }
}
