package com.company;

public class Kerucut {

    double jarikerucut;
    double tinggikerucut;
    double lpkerucut;
    double volkerucut;
    double selimutkerucut;

    public double getJarikerucut() {
        return jarikerucut;
    }

    public void setJarikerucut(double jarikerucut) {
        this.jarikerucut = jarikerucut;
    }

    public double getTinggikerucut() {
        return tinggikerucut;
    }

    public void setTinggikerucut(double tinggikerucut) {
        this.tinggikerucut = tinggikerucut;
    }

    public double getLpkerucut() {
        return 3.14 * getJarikerucut() * (getJarikerucut() + getSelimutkerucut());
    }

    public double getVolkerucut() {
        return (3.14 * getJarikerucut() * getJarikerucut() * getTinggikerucut()) / 3;
    }

    public double getSelimutkerucut() {
        return Math.sqrt(getJarikerucut() * getJarikerucut() + getTinggikerucut() * getTinggikerucut());
    }
}
