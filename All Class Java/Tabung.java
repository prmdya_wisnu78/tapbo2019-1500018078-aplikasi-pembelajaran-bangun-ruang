package com.company;

public class Tabung {

    double jaritabung;
    double tinggi;
    double voltabung;
    double lptabung;

    public double getJaritabung() {
        return jaritabung;
    }

    public void setJaritabung(double jaritabung) {
        this.jaritabung = jaritabung;
    }

    public double getTinggi() {
        return tinggi;
    }

    public void setTinggi(double tinggi) {
        this.tinggi = tinggi;
    }

    public double getVoltabung(){
        return 3.14 * getJaritabung() * getJaritabung() * getTinggi();
    }

    public double getLptabung() {
        return 2 * 3.14 * getJaritabung() * (getJaritabung() + getTinggi());
    }
}
