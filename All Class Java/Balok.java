package com.company;

public class Balok {
    double pbalok;
    double lbalok;
    double tbalok;
    double lpbalok;
    double volbalok;

    public double getPbalok() {
        return pbalok;
    }

    public void setPbalok(double pbalok) {
        this.pbalok = pbalok;
    }

    public double getLbalok() {
        return lbalok;
    }

    public void setLbalok(double lbalok) {
        this.lbalok = lbalok;
    }

    public double getTbalok() {
        return tbalok;
    }

    public void setTbalok(double tbalok) {
        this.tbalok = tbalok;
    }

    public double getLpbalok() {
        return ((2 * getPbalok() * getLbalok()) + (2 * getLbalok() * getTbalok()) + (2 * getPbalok() * getTbalok()));
    }

    public double getVolbalok() {
        return getPbalok() * getLbalok() * getTbalok();
    }
}
