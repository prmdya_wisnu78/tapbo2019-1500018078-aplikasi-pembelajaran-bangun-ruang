package com.company;

public class Bola {

    double jaribola;
    double lpbola;
    double volbola;

    public double getJaribola() {
        return jaribola;
    }

    public void setJaribola(double jaribola) {
        this.jaribola = jaribola;
    }

    public double getLpbola() {
        return 4 * 3.14 * getJaribola() * getJaribola();
    }

    public double getVolbola() {
        return 4 * (3.14 * getJaribola() * getJaribola() * getJaribola()) / 3;
    }
}
