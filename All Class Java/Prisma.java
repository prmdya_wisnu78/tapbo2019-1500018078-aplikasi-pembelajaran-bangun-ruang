package com.company;

public class Prisma {
    double salasa;
    double salasb;
    double tprisma;
    double volprisma;
    double lpprisma;
    double lalasprisma;
    double lpersegiprismaa;
    double lpersegiprismab;
    double lpersegiprismac;
    double smiringalas;

    public double getSalasa() {
        return salasa;
    }

    public void setSalasa(double salasa) {
        this.salasa = salasa;
    }

    public double getSalasb() {
        return salasb;
    }

    public void setSalasb(double salasb) {
        this.salasb = salasb;
    }

    public double getTprisma() {
        return tprisma;
    }

    public void setTprisma(double tprisma) {
        this.tprisma = tprisma;
    }

    public double getSmiringalas() {
        return (Math.sqrt(getSalasa() * getSalasa() + getSalasb() * getSalasb()));
    }

    public double getLalasprisma() {
        return ((getSalasa() * getSalasb()) / 2);
    }

    public double getLpersegiprismaa() {
        return getSalasa() * getTprisma();
    }

    public double getLpersegiprismab() {
        return getSalasb() * getTprisma();
    }

    public double getLpersegiprismac() {
        return getSmiringalas() * getTprisma();
    }

    public double getVolprisma() {
        return (((getSalasa() * getSalasb())/2) * getTprisma());
    }

    public double getLpprisma() {
        return ((2 * getLalasprisma()) + (getLpersegiprismaa() + getLpersegiprismab() + getLpersegiprismac()));
    }
}
