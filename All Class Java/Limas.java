package com.company;

public class Limas {

    double salaslimas;
    double tlimas;
    double ssegitigaa;
    double ssegitigab;
    double lsegitiga;
    double vollimas;
    double lplimas;

    public double getSalaslimas() {
        return salaslimas;
    }

    public void setSalaslimas(double salaslimas) {
        this.salaslimas = salaslimas;
    }

    public double getTlimas() {
        return tlimas;
    }

    public void setTlimas(double tlimas) {
        this.tlimas = tlimas;
    }

    public double getSsegitigaa() {
        return getSalaslimas();
    }

    public double getSsegitigab() {
        return ssegitigab;
    }

    public void setSsegitigab(double ssegitigab) {
        this.ssegitigab = ssegitigab;
    }

    public double getLsegitiga() {
        return ((getSalaslimas() * (Math.sqrt((getSsegitigab()*getSsegitigab())+((getSalaslimas()/2)*(getSalaslimas()/2)))))/2);
    }

    public double getVollimas() {
        return (((getSalaslimas() * getSalaslimas())* getTlimas()) / 3);
    }

    public double getLplimas() {
        return ((getSalaslimas() * getSalaslimas()) + (4 * getLsegitiga()));
    }
}
